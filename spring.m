clear
%G taken from https://www.engineeringtoolbox.com/modulus-rigidity-d_946.html 
G = 79.3e9; %structural steel rigidity modulus
r = 2e-3; %spring radius 
R = 2 * 0.0254; %spring coil radius
m = 10e-3; %mass
%g constant taken from https://physics.nist.gov/cgi-bin/cuu/Value?gn|search_for=gravitational+acceleration
g = 9.80665; 
F_0 = m * g;
tau = 2; %damping time constant
beta = 1 / 2 * tau ; %damping factor
omega = 50:0.5:400; %frequency
alpha_0 = g;
N = 2:50; %list of number of spring coils

fig = figure('Name','Graph','NumberTitle','off'); % creating figure window

subplot(2,1,1); %creating axes in tiled positions
color = ['r','g','b','c','m','ky']; %color list for plots

file_ID = fopen('resonance_analysis_results.dat', 'w'); %creating new file for writing
fprintf(file_ID,'G = %.2d [Pa], r = %.0d [m], R = %.3f [m], m = %.2f [kg], g = alpha = %.2f [m/s^2], tau = %d [s] \n\n', ...
 G, r, R, m, g ,tau); %writing constant values to file

hold on; %saving current charts when adding new ones

%calculating  amplitude depending on the number of coils for different frequencies
for p=1:size(N,2)
  for t=1:size(omega,2)
      k = G * r^4/(4 * N(p) * R^3);
      omega_0 = sqrt(k / m);
      y(t) = alpha_0 / sqrt((omega_0^2 - omega(t)^2)^2 + 4 * beta^2 * omega(t)^2); 
  end
  plot(omega / (2*pi), y, color(mod(p,6)+1)); %creating a plot
  xlim([8, 60]); % setting limit for X axis
end
hold off;

%setting title and labels for X and Y axis
title('A(f) for diffrent N');
ylabel('Amplitude of forced vibration A, m');
xlabel('Frequency f, Hz');

subplot(2,1,2); 
%calculating resonance amplitude depending on the number of coils
for p=1:size(N,2)
  k = G * r^4/(4 * N(p) * R^3);
  omega_0 = sqrt(k / m);
  A(p) = alpha_0 / sqrt((4 * beta^2 * omega_0^2));
  fprintf(file_ID,'N = %d, A(N) = %.3f [m], f = %.3f [Hz] \n', N(p), A(p), omega_0); %writing data to file
end

fclose(file_ID); %closing file

%setting title and labels for X and Y axis
plot(N,A);
title('A(N)');
xlabel('Number of spring coils N');
ylabel('Resonance amplitude A, m');

saveas(fig, 'resonance.pdf'); % saving figure to pdf format




